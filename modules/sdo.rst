﻿Добавление и работа с регистрами SDO. Пример на модуле BRIC-AO-4.
=================================================================
.. note::
    Данный урок показан на примере модуля расширения BRIC-AO-4. Аналогичным образом добавляются и используются регистры SDO и для остальных модулей.

Для проверки регистров SDO модуля расширения BRIC-AO-4 напишем программу на языке FBD.
Добавляем модуль AO в Beremiz, и откроем окно настроек. Все регистры, кроме AO_ao_val_х являются регистрами SDO.

.. figure:: img/9.png
    :width: 600
    :align: center

    *SDO регистры в AO*

Наша программа будет записывать новый адрес modbus, а также читать CANopen-адрес модуля расширения.  В окне регистров модуля расширения находим SDO-регистры AO_1_mdb_addr и AO_1_module_number. В столбце Polling для первого регистра прописываем «write», а для второго «read».

.. figure:: img/10.png
    :width: 600
    :align: center

    *Запись в Polling SDO регистров в AO*

Добавим регистры в основную программу. 

.. figure:: img/11.png
    :width: 600
    :align: center

    *Программа для регистров SDO*

Загружаем программу в ПЛК и заходим на WEB-страницу контроллера через Ethernet. Также зайдем на WEB-страницу модуля расширения через USB-порт.

.. figure:: img/12.png
    :width: 600
    :align: center

    *WEB-страница ПЛК*

.. figure:: img/13.png
    :width: 600
    :align: center

    *WEB-страница модуля расширения*

Как видно на странице модуля, modbus-адрес поменялся на «0», так как по умолчанию «write_mdb» инициализируется нулевым значением. Также на этой странице можно увидеть, что «module_number» равно 1, как и полученное значение переменной «read_number». При изменении значения переменной «write_mdb», например на «3», изменится и «mdb_addr» модуля.

.. figure:: img/14.png
    :width: 600
    :align: center

    *WEB-страница ПЛК*

.. figure:: img/15.png
    :width: 600
    :align: center

    *WEB-страница модуля расширения*

Напишем еще одну программу, которая позволит управлять токовым выходом модуля расширения BRIC-AO-4 в соответствии с вводимым значением в единицах физической величины (mA). Для этого, сначала необходимо перевести вводимую физическую величину AO_out_x в единицы ЦАП из диапазона 0-4095, а затем, полученное значение AO_val_x необходимо передать в один из PDO-регистров AO_1_ao_val_x модуля расширения.

Расчет значения AO_val_x в единицах ЦАП осуществляется по формуле:

    AO_val_x = (AO_out_x - AO_calib_b_x) * AO_calib_a_x;

где AO_out_x – вводимое значение тока (напряжения) выходного канала в mA (Вольтах);

AO_calib_a_x и AO_calib_b_x - индивидуальные калибровочные коэффициенты каждого канала. 

В SDO-регистрах AO_1_ao_calib_a_x и AO_1_ao_calib_b_x модуля 1.1.х:АО_1 хранятся значения индивидуальных калибровочных коэффициентов AO_calib_a_x и AO_calib_b_x соответственно. Важно отметить, что для считывания значений калибровочных коэффициентов требуется установить статус read в поле Polling соответствующих SDO-регистров.
Для первого выходного канала модуля 1.1.х:АО_1 программа будет иметь следующий вид: 

.. figure:: img/45.png
    :width: 600
    :align: center

    *Программа на языке FBD*

После загрузки в ПЛК заходим в WEB-страницу и задаем какое-либо значение в mA в поле "value", например, "12".

.. figure:: img/46.png
    :width: 600
    :align: center

    *Параметр "value"*

На модуле расширения в нулевом канале будет задаваться аналоговый сигнал, равный 12mA. Данной программой мы можем перерасчитывать в значения ЦАП значения вводимых mA.

.. note:: 
    Подробно о модуле расширения BRIC-AO-4 можно узнать по ссылке_

.. _ссылке: https://bric-ao.readthedocs.io/ru/latest/index.html
