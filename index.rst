.. bric_plc documentation master file, created by
   sphinx-quickstart on Sun Jun 14 16:52:51 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Обучающие уроки по программированию в среде Beremiz
===================================================
Среда разработки «Beremiz» предназначена для создания и отладки прикладных программ на языках стандарта IEC 61131-3. В качестве ПЛК в данных уроках будет использоваться BRIC и его модули расширения – разработка ООО "СНЭМА-СЕРВИС". В качестве языков описания алгоритмов и логики работы данных программ будут выступать Structured Text (далее ST) и Function Block Diagram (далее FBD).


.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   intro/index
   channels/index
   modules/index  
   modbus/index
   archieve/index

