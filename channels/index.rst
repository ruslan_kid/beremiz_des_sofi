﻿Каналы ввода-вывода
===================

.. toctree::
   :maxdepth: 2
   :caption: Содержание:

   di_state
   di_cnt_freq
   do_state
   do_di
   do_state_ctrl
   do_lights
   do_pwm
   ai_reg
   ai_temp
   ai_do
   pump_station

   