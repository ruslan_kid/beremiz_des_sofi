﻿DO. Обратная связь (обрыв, короткое замыкание), управление защитой от короткого замыкания
=========================================================================================
В Beremize для управления защитой от короткого замыкания дискретных выходов ПЛК BRIC реализован функциональный блок WRITE_DO_SC. При наличии короткого замыкания загорается красный индикаторный светодиод, соответствующий канал DO отключается, управление каналом блокируется и выставляется флаг DO_SC. При наличии обрыва на линии дискретных выходов красный светодиод мигает.

Напишем программу на языке FBD. В ней мы будем проверять дискретные выходы DO_0 и DO_1 на КЗ. Программа выглядит следующим образом.

.. figure:: images/39.png
    :width: 600
    :align: center

Если на линии выходов DO_0 и DO_1 нет обрывов, то загораются зеленые индикаторы на каналах.

.. figure:: images/plc_sc2.gif
    :width: 600
    :align: center


    *Визуализация программы на ПЛК*

Включаем программную защиту от короткого замыкания с помощью переменной "enable" .

.. figure:: images/40.png
    :width: 600
    :align: center


    *Web-страница программы*

Далее делаем КЗ одновременно на двух каналах DO_0 и DO_1 с помощью перемычки, замыкая их на "DO_VCC". В результате срабатывает программная защита, каналы отключаются и устанавливается флаги SC Flag, загораются красные светодиоды и управление соответствующими каналами блокируется.

.. figure:: images/plc_sc1.gif
    :width: 600
    :align: center


    *Визуализация программы на ПЛК*

Сбросить флаг можно записав в переменную "Flag" значение "0".

Если оборвать цепь на каком либо задействованном дискретном выходе, то красный светодиод соответсвующего канала начинает мигать. В нашей программе протестируем на канале DO_0.

.. figure:: images/plc_sc3.gif
    :width: 600
    :align: center


    *Визуализация программы на ПЛК*

.. seealso:: 
    Подробно о дискретных выходах ПЛК BRIC можно узнать по ссылке_

.. _ссылке: https://bric-plc.readthedocs.io/ru/latest/digital_outputs.html
