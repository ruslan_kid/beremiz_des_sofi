﻿DO. Управление каналом DO по состоянию канала DI
================================================
Для данного урока подготовим ПЛК BRIC: соединяем DO_GND и DI_COM, DO_0 -> DI_0, DO_1 -> DI_1.

.. figure:: images/plc_6.gif
    :width: 600
    :align: center

    *Подключение каналов ПЛК BRIC*

.. hint::

    В данном случае каналы DO работают как сухой контакт

Напишем программу на языке ST для управления каналами DO_0 и DO_1 по состоянию дискретных входов. В переменную *di_out* записывается состояние дискретных входов (READ_DI). Реализация программы представлена ниже:

.. figure:: images/35.png
    :width: 600
    :align: center

    *Программа на языке ST*

Данная программа записывает вначале цикла значение логической "1" в канал DO_0 с разрешающей маской "15"(все каналы). Так как DO_0 соединен с DI_0 состояние DI_STATE будет равно "1". Далее программа проверяет состояние дискретных входов, если оно равно "1", то записываем в канал DO_1 логическую "1" (DO_VALUE = 2). Таким же образом проверяется состояние каналов DI, и если оно равно "2", то цикл начинается заново.
Цикл программы сделаем равным 1 секунде.

.. figure:: images/36.png
    :width: 600
    :align: center

    *Цикл программы*

После загрузки прошивки в контроллер с цикличностью в 1 секунду поочередно начинают гореть зеленые индикаторные светодиоды каналов DO_0-DI_0 и DO_1-DI_1.


.. figure:: images/plc_di_do.gif
    :width: 600
    :align: center

    *ПЛК BRIC после реализации программы*

Таким образом, в данной программе видно, что состояние каналов DO меняется при изменении состояния каналов DI.

.. seealso::  
    Подробно о дискретных выходах ПЛК BRIC можно узнать по ссылке_

.. _ссылке: https://bric-plc.readthedocs.io/ru/latest/digital_outputs.html