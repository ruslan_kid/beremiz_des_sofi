﻿DI. Подсчет импульсов, измерение частоты, тонкая настройка
===========================================================
Для подсчета импульсов дискретных входов ПЛК BRIC в среде Beremiz разработан функциональный блок READ_DI_CNT, а для измерения частоты – READ_DI_FREQ.
Создаём программу на языке FBD. Функциональные блоки READ_DI_CNT и READ_DI_FREQ добавляются из "Library" во вкладке "DI Function Blocks".

С помощью кнопки |var| из панели навигации добавляем, например, константу "11", чтобы прочесть состояние с канала DI_11.

 .. |var| image:: images/19.png

.. figure:: images/18.png
    :width: 600
    :align: center

    *Программа для подсчета импульсов и измерения частоты*

Подключаем дискретный канал DO_3 c каналом DI_11 согласно схеме, представленной ниже.

.. figure:: images/2.gif
    :width: 600
    :align: center

Скомпилируем программу и загрузим в ПЛК.
После загрузки программы в ПЛК BRIC заходим в WEB-страницу контроллера по URL 192.168.1.232_ (URL можно поменять в настройках ip address в WEB-странице).

.. _192.168.1.232: http://192.168.1.232/


Получаем данные количества импульсов и частоты канала DI_11. Изначально они равны "0".

.. figure:: images/20.png
    :width: 600
    :align: center

    *Исходные данные количества импульсов и частоты*

Далее включаем режим ШИМ на канале DO_3 через WEB-страницу контроллера.

.. hint:: Включение режима ШИМ на DO_3
    
    .. figure:: images/3.gif
        :width: 600
        :align: center


Заходим в раздел Пользовательских переменных и смотрим изменения *di_impulse_cnt* и *di_frequence*

.. figure:: images/21.png
    :width: 600
    :align: center

    *Данные количества и частоты после появления импульсного сигнала*

В данном случае количество импульсов инкрментируется с фиксированной частотой 20 Гц.

.. figure:: images/plc_di.gif
    :width: 600
    :align: center

    *Импульсный сигнал DI_11 на ПЛК BRIC*

**Тонкая настройка**

В среде Beremiz для ПЛК BRIC разработаны фунциональные блоки WRITE_DI_NOISE_FLTR_10US, WRITE_DI_PULSELESS, WRITE_DI_MODE для тонких настроек дискретных входов.

Функциональный блок WRITE_DI_NOISE_FLTR_10US для указанного дискретного входа задает минимальную длительность входящего импульса. Все что меньше данного значения будет воспринято контроллером как помеха и не будет обрабатываться.

Функциональный блок WRITE_DI_PULSELESS для указанного дискретного входа задает время обнуления измеренной частоты. Если в течение это времени не поступило ни одного импульса, измеренное значение di_freq обнуляется.

Функциональный блок WRITE_DI_MODE для указанного дискретного входа обозначает подключенные опции (0 – не подключены, 1 – подключен счетчик импульсов, 2 – подключен расчет частоты дискретного входа, 3 – подключен счетчик импульсов и расчет частоты дискретного входа).

Создаём программу на языке FBD. Для канала DI_0 установим минимальную длительность импульса 3*10мкс = 30 мкс. Для DI_1 установим время обнуления измерения частоты - 20000 мс (20 сек). Для DI_2 запишем код подключенных функций данного канала - 1 (счетчик импульсов).

.. figure:: images/22.png
    :width: 600
    :align: center

    *Программа для тонкой настройки дискретных входов*

Результат загруженной программы можно посмотреть в WEB-странице ПЛК во вкладке "Дискретные входы (Discrete inputs)". Как видно на рисунках ниже, изменения внесены:

.. figure:: images/23.png
    :align: center

    *Период нечувствительности импульса*

.. figure:: images/24.png
    :align: center

    *Время обнуления измеренной частоты*

.. figure:: images/25.png
    :align: center

    *Подключенная функция - подсчет импульсов*

.. seealso:: 
    Подробно о дискретных входах ПЛК BRIC можно узнать по ссылке_

.. _ссылке: https://bric-plc.readthedocs.io/ru/latest/digital_inputs.html
