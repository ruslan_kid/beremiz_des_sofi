﻿Архив с циклической записью данных
==================================
В данном уроке ознакомимся с архивированием данных с циклической записью. Напишем 2 программы с разными зависимостями от циклов:

    * цикл самой программы (resource)

    * цикл от часов реального времени ПЛК (RTC).

Для первого варианта напишем программу на языке ST. Добавим локальную булеву переменную *arc_saver*. Также из окна регистров архива добавим *Arch_save_arc*. В паре с переменной *arc_saver*  они будут являться "триггером" записи данных в архив. Программа будет записывать итерацию переменной с циклом в 1 минуту. Программа имеет следующий вид:

.. figure:: img/2.png
    :width: 600
    :align: center

    *Программа archive_resource*

Для того, чтобы запись происходила 1 раз в минуту, необходимо задать *task* с интервалом в 30 секунд в ресурсах, так как в программе идет переключение из "0" в "1" за 1 цикл и для обратного переключения необходим дополнительный цикл.

.. figure:: img/3.png
    :width: 600
    :align: center

    *Ресурс программы*

В окне регистров архива "Arch_0" добавим переменную *value1_arch* с типом данных "UINT".

.. figure:: img/4.png
    :width: 600
    :align: center

    *Окно регистров архива*

После загрузки программы в ПЛК инкрементируемые данные переменной *value1* будут записываться в архив с периодичностью 1 раз в минуту.

Для второго варианта напишем программу на языке FBD. В данном случае будем привязываться к часам реального времени (RTC). Из "Library" добавим функциональный блок STRUCT_REAL_TIME. Переменную *minute_trigger* определим как кратную 1 минуте. От ее состояния определяется запись данных в архив. Добавим подмодуль "Arch_1". В программу копируем регистр *Arch_1_save_arc*.
Также в программу добавим инкрементируюмую переменную *value2* и записываюмую в архив *value2_arch*. Конечный вариант программы показан на рисунке ниже:

.. figure:: img/5.png
    :width: 600
    :align: center

    *Программа archive_rtc*

В ресурсах добавляем *task* равной 25 мс и добавляем в "Instances".

.. figure:: img/6.png
    :width: 600
    :align: center

    *Ресурс программы*

В окне регистров архива "Arch_1" добавим переменную *value2_arch* с типом данных "UDINT".

.. figure:: img/7.png
    :width: 600
    :align: center

    *Окно регистров архива*

После загрузки программы в ПЛК инкрементируемые данные переменной *value2* будут записываться в архив с периодичностью 1 раз в минуту.